# Pourquoi libérer la monnaie ?

C'est vrai ça, elle nous a rien fait, d'abord...  
... si ?

## Diapo 1 : à quoi ça sert la monnaie ?

1. Échanges

![](https://cdn.pixabay.com/photo/2016/04/06/11/23/trade-1311577_960_720.jpg)

1. Valeur

\(unité de compte\)

![](https://cdn.pixabay.com/photo/2016/02/27/00/26/tape-measure-1224958_960_720.png)

1. Tésorisation

\(stockage, dragon sur son tas d'or\)

![](https://orig00.deviantart.net/f93f/f/2011/364/8/0/805232ce467d50cb20a8ac4a1df7d798-d4kpejn.jpg)

## Diapo 2 : on crée régulièrement de la monnaie :

* 78k € / citoyen
* +10% / an, ~
* 655€/mois/pers soit 22 € /j/pers.
* ## Diapo 3 : Comment crée-t-on de la monnaie ?

euro \(monnaie-dette\)![](https://pbs.twimg.com/media/CKX5F1oUYAAVLg0.jpg)

bitcoin \(POW : minage comme l'or, mais de façon informatique\) 

![](https://www.ophtek.com/wp-content/uploads/2014/02/bitcoin-miner.jpg)

## Diapo 4 : Et on a le droit ? Et les impôts ?

* Dérégulation = on a les droits

* les monnaies complémentaires = autogestion

* Impôts : marchandise tant que pas de reconnaissance étatique

![](https://steigerlegal.ch/wp-content/uploads/2011/06/mimiandeunice_allowed_001_600.png)

## Diapo 5 : Ça existe, les monnaies libres ?

=&gt;  Une : réseau Duniter monnaie June, exemple ici.

