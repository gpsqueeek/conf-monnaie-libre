# La monnaie libre quand on n'y connaît que pouic

Conférence de vulgarisation sur la monnaie libre, le réseau Duniter et la monnaie Ğ1 \(prononcer « June »\).

## But

Intéresser/informer un public néophyte dans les domaines mathématiques, monétaires ou informatiques, qui néanmoins apprécie les alternatives sociales

Parler de la monnaie libre du point de vue d'utilisateur·trice·s, en évitant les démonstrations mathématiques ou informatiques \(que les spécialistes font déjà très bien\).

## Format

* Ceci est un gitbook, outil git propice à la contribution
* 1h \(45mn de conf, 15mn de questions/réponses\)
* 2 personnes \(plus vivant\), mais adaptable pour 1
* Assumer l'approche de surface : si on veut creuser tel ou tel sujet, des spécialistes le font très bine \(cf. sources, à approfondir\)

# Sources

_\(à travailler\)_

* Theorie Relative de la Monnaie : [http://trm.creationmonetaire.info/](http://trm.creationmonetaire.info/)
* TRM pour les enfants : [http://cuckooland.free.fr/LaTrmEnCouleur.html](http://cuckooland.free.fr/LaTrmEnCouleur.html)
* FAQ de duniter.org : [https://duniter.org/fr/faq/](https://duniter.org/fr/faq/)
* infos monnaie dette : [http://www.creationmonetaire.info/](http://www.creationmonetaire.info/)

  








